bidiv (1.5-5) unstable; urgency=medium

  * Remove Baruch Even from Uploaders (Closes: #759996)
  * Compat level 10
  * Secure VCS-Broser URL
  * Show branch trunk in VCS URLs
  * Enable all hardening options
  * A DEP-5 copyright file
  * Multi-Arch: foreign
  * Standard-Version 3.9.8

 -- Tzafrir Cohen <tzafrir@debian.org>  Tue, 24 Jan 2017 22:29:04 +0200

bidiv (1.5-4) unstable; urgency=low

  [ Baruch Even ]
  * Add Vcs-* fields

  [ Lior Kaplan ]
  * Update debian/copyright with some missing details (Closes: #458601)
  * Switch to dpkg-source 3.0 (quilt) format

  [ Tzafrir Cohen ]
  * Patch fribidi_019: Fix bidiv for fribidi 0.19 (Closes: #568130, #571351).
  * Add myself as uploader.
  * Standards version 3.8.4 (no change needed).
  * Correct path to license file in debian/copyright.
  * Missing ${misc:Depends} debhelper deps.
  * Patch hyphen_minus: hyphen/minus in man page (originally in diff).
  * Patch term_size_get: properly check terminal width (originally in diff).
  * Patch makefile: simple Makefile fixes (originally in diff).
  * Patch try_utf8_fix: if-def out a variable that is unused if TRY_UTF8
    (originally in diff).
  * Patches cast_fix, type_fix: fix build warnings.

 -- Tzafrir Cohen <tzafrir@debian.org>  Tue, 25 May 2010 07:06:07 +0300

bidiv (1.5-3) unstable; urgency=low

  * Update to Standards-Version 3.7.3, no changes needed
  * Switch to compat level 5, updated debhelper dependency as well
  * Disable the watch file, ivrix.org.il domain is down for an extended period
    of time (Closes: #449917)

 -- Baruch Even <baruch@debian.org>  Sat, 29 Dec 2007 13:36:41 +0200

bidiv (1.5-2) unstable; urgency=low

  * Adjust makefile due to cdbs changes, cdbs overrides the LDFLAGS of the
    package thus depriving us from the output of `fribidi-config --libs`.
    Closes: #376470.
  * Update Standards-Version to 3.7.2, no changes needed.

 -- Baruch Even <baruch@debian.org>  Mon,  3 Jul 2006 08:44:43 +0000

bidiv (1.5-1) unstable; urgency=low

  * New upstream version

 -- Baruch Even <baruch@debian.org>  Sat,  7 Jan 2006 22:21:00 +0000

bidiv (1.4-6) unstable; urgency=medium

  * Apply patch by Shachar Raindel <shacharr@gmail.com> regarding insufficient 
    memory allocation when handling Unicode strings. (Closes: #346386)
  * Fix man page error reported by Lintian.

 -- Lior Kaplan <webmaster@guides.co.il>  Sat,  7 Jan 2006 17:54:54 +0200

bidiv (1.4-5) unstable; urgency=low

  * Change maintainership to Debian-Hebrew group.
  * Add Lior Kaplan as Uploader.
  * Change standards-version to 3.6.2, no changes needed.

 -- Baruch Even <baruch@debian.org>  Sat, 30 Jul 2005 20:57:26 +0100

bidiv (1.4-4) unstable; urgency=low

  * Rebuild against fribidi 0.10.4-6, 0.10.4-5 had the wrong shlibs file.
    (Closes: #278790)

 -- Baruch Even <baruch@debian.org>  Thu, 28 Oct 2004 13:22:33 +0100

bidiv (1.4-3) unstable; urgency=low

  * Recompile to build with fixed libfribidi0 to work on 64 bit machines

 -- Baruch Even <baruch@debian.org>  Wed, 27 Oct 2004 12:52:36 +0100

bidiv (1.4-2) unstable; urgency=low

  * Rebuild to have proper dependency on libfribidi0.
  * Fix some minor warnings in the code.
  * Update to latest policy version.
  * Change manpage to use correct escaping of minus-signs

 -- Baruch Even <baruch@debian.org>  Mon, 11 Oct 2004 05:28:09 +0100

bidiv (1.4-1) unstable; urgency=low

  * Initial Release.
  * Get terminal size instead of using the fixed 80 or COLUMNS
    we now get the terminal size and fallback to COLUMNS.

 -- Baruch Even <baruch@debian.org>  Tue, 31 Dec 2002 11:26:50 +0200

